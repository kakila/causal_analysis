# Causal Analysis Toolbox

Tools for causal exploration, calculus, discovery, and inference.

## Roadmap

- [ ] Representation and manipulation of causal graphs (i.e. the graph
supporting the SCM)
  - [ ] representation of mixed graphs with bidirected edges
  - [ ] manipulation of mixed graphs: marginalizations, interventions,
subgraph, graph union, etc ...
  - [ ] Analysis of mixed graphs: parents, childs, ancestors, confounder
set, v-structures, d-separation, adjustment sets,
(backdoor, frontdoor, fwd. nec., ...) criterion.
  - [ ] Equivalent classes based on graphs only.
- [ ] Sampling from SCM (cyclic)
- [ ] Identification of equivalent classes, graph and mechanisms.
- [ ] Solvavility
- [ ] Analytic marginalization
- [ ] Generation of graphs from structural equations, i.e. from
mechanisms to full SCM
- [ ] Identification of SCM from data.
  - [ ] Identification of the graph structure
  - [ ] Identification of the mechanism (parametric or nonparmetric)
